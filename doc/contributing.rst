.. highlight:: shell-session

Contributing
============

If you would like to contribute to the ligo-raven project via development or
reporting issues, please refer to the `issues page`_.

Code changes should be made via merge requests that are reviewed and pass all
unittests (including new ones that may need to be added). If you aren't
familiar with git or want a refresher for specific steps, refer to the below
step-by-step guide to making code changes.

Setting up your local repository
--------------------------------

Firstly, you will want to request permission to make changes to the
`git repository`_ by becoming a member via
**Project Information => Members**.
The next step is to make a personal fork of the ligo-raven project and clone
this to your local computer. This is done by finding the fork button on
the `git repository`_ and following the directions.

.. image:: _static/make_fork_1.png
   :alt: Fork button

.. image:: _static/make_fork_2.png
   :alt: Create own fork
   :scale: 50 %

Next we can clone this fork to where you want to work, usually a local
computer or home directory on a cluster. This can be done via ssh or
https using the instructions **on your fork** (use the https method if
you haven't set up ssh keys with gitlab). Cloning your fork ensures that
it is set as the default origin fork rather than the main repo,
potentially avoiding accidental pushes to the wrong repo.

.. image:: _static/clone_repo.png
   :alt: Download fork

For example the https method is::

    $ git clone https://git.ligo.org/albert.einstein/raven.git

Once you have your local fork downloaded, enter it and set the main
`git repository`_ as the upstream::

    $ cd raven
    $ git remote add upstream https://git.ligo.org/lscsoft/raven/

We can check we have both our origin and upstream forks set up correctly
by using the command::

    $ git remote -v
    origin https://git.ligo.org/albert.einstein/raven (fetch)
    origin https://git.ligo.org/albert.einstein/raven (push)
    upstream https://git.ligo.org/lscsoft/raven (fetch)
    upstream https://git.ligo.org/lscsoft/raven (push)

Next, we should make sure we are able to incorporate the latest changes by
using the fetch/rebase method::

    $ git checkout master
    $ git fetch upstream
    $ git rebase upstream/master

Making changes
--------------

We should always make a new branch if we want to make changes to the code::

    $ git checkout -b my-new-feature upstream/master

We can check we are on a new branch by::

    $ git branch
    master
    * my-new-feature

Now we can make changes to existing files as we please.

Once we are done making changes, we can make a commit and include all file
changes by::

    $ git commit -a

and giving this commit a name descriptive of the changes. If these changes 
include new files, we need to add these before making a commit::

    $ git add path/to/newfile

We can push these changes to our own fork (origin) by::

    $ git push origin

Finally, we can make a merge request, by going to **Merge Requests** on your
local fork or click **Create merge request** via a pop-up prompt. Follow the
instructions and include a description of your changes.

.. image:: _static/merge_request.png
   :alt: Make merge request
   :scale: 50 %

Make sure you also document your changes in CHANGES.rst and include this in
your merge request.

Incorporating recent upstream or local changes
----------------------------------------------

If other changes are made and merged before your merge
request is finished, the you will need to use incorporate these changes using
the fetch/rebase method::

    $ git fetch upstream
    $ git rebase upstream/master
    $ git push origin -f

If there is a conflict, fix the conflict in the appropirate file(s)
(remembering to remove the extra text added), add the file(s) and then
continue the rebase::

    $ git add path/to/file-w-conflict
    $ git rebase --continue

Continue this until every conflict has been resolved.

Also, your merge request should only containt a single commit before merging,
where you can squash all your commits to the first one by::

    $ git rebase -i upstream/master

and replace every **pick** except the first with **f**.

Once your changes are reviewed and improved, and pass all tests, it can be
merged in the main `git repository`_ and be included in the next
:doc:`release <release>`.

.. _`issues page`: https://git.ligo.org/lscsoft/raven/-/issues
.. _`git repository`: https://git.ligo.org/lscsoft/raven/
